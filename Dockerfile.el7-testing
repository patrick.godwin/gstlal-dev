# This is an attempt to revamp the optimized gstlal development 
# container. The base is the lalsuite-development container. 

FROM containers.ligo.org/alexander.pace/mkl-dev/mkl-dev:el7

# Labeling/packaging stuff:
LABEL name="GstLAL Development Package, EL7 Testing" \
      maintainer="Alexander Pace <alexander.pace@ligo.org>" \
      date="2020-05-05" \
      support="Reference Platform, EL7"

USER root

## Copy Optimized RPMs to container
COPY rpms /rpms

## Copy lalsuite patches to container
COPY patches /patches

## Add lscsoft-backports-testing repo. This is a temporary
## step before packages go into production.

RUN sed 's/production/backports-testing/g' /etc/yum.repos.d/lscsoft-production.repo > /etc/yum.repos.d/lscsoft-backports-testing.repo

# Install Optimized RPMs, delete old RPMs
RUN yum makecache && \
      yum -y localinstall /rpms/*.rpm 

# Apply lalsuite patches. Note: patches must be formatted such that they
# can be executed in the / directory to files already installed on the 
# system (e.g., /usr/lib64/...). Less than ideal, i'll fix this later. 

RUN cd / && \
    for f in $(ls patches/*); do patch -p0 -i $f; done
      
# Copy spec file patching script:
RUN chmod +x /rpms/patch_optimized_spec_file
RUN cp /rpms/patch_optimized_spec_file /usr/bin/


## Install development components for gstlal.

RUN yum -y install python2-ligo-lw python36-ligo-lw

# Install gstreamer and associated packages. I yum info'd gstreamer1, and 
# it was installing from lscsoft-backports. So I think it should be 
# good to go. 

# FIXME: I'm admitting defeat on this for right now. Remove
# python2-gstreamer1 until I can resolve the conflict with
# gst-inspect-1.0 using python2 first...

RUN yum -y install gstreamer1 \
		   gstreamer1-devel \
		   gstreamer1-plugins-base-devel \
		   gstreamer1-plugins-good \
		   gstreamer1-plugins-base-devel \
		   python3-gstreamer1 \
		   gstreamer1-plugins-bad-free \
		   gobject-introspection \
		   gobject-introspection-devel \
		   glib2-devel \
		   pygobject3-devel 

# Install GDS for online

RUN yum -y install gds-core \
		   gds-lowlatency \
		   gds-lowlatency-headers \
                   gds-lowlatency-devel \
                   gds-crtools
           
# Try installing gstlal-ugly dependencies
RUN yum -y install ldas-tools-framecpp \
                   ldas-tools-framecpp-c \
                   ldas-tools-framecpp-c-devel \
                   ldas-tools-framecpp-devel \
                   ldas-tools-framecpp-doc \
                   ldas-tools-framecpp-swig \
		   gds-devel \
		   nds2-client-devel \
                   nds2-client-headers

# Install gstlal dependendies:

RUN yum -y install gtk-doc \
                   graphviz 

RUN yum -y install python2-ligo-scald python36-ligo-scald

# Install avahi-daemon
RUN yum -y install avahi \
                   avahi-libs \
                   avahi-ui-gtk3 \
		   avahi-ui-tools \
                   avahi-autoipd

# Install support packages
RUN yum -y install dqsegdb \
                   vim

# I think these changing these avahi daemon flags accomplish two
# goals:

# This allows for multiple instances of avahi *with the same PID*
# to run on the same machine simultaneously. This necessary for when
# different CI pipelines run on at the same time on the gitlab runner 
# machine. I could also see this being an issue if multiple containers 
# get spun up on one computer. You don't see this issue run running 
# multiple instances of gstlal on one machine because there's only one
# avahi-daemon running:

RUN sed -i 's/rlimit-nproc=3/rlimit-nproc=333/g' /etc/avahi/avahi-daemon.conf

# This allows for avahi to run without dbus. I think this was a security
# thing for CentOS containers.

RUN sed -i 's/#enable-dbus=yes/enable-dbus=no/g' /etc/avahi/avahi-daemon.conf

# Clean up and close-out

RUN rm -rf /tars /src /rpms && \
    yum clean all

# Build gstreamer registry cache:

# RUN gst-inspect-1.0

# Export MKL environment variables: 

ENV MKL_INTERFACE_LAYER LP64
ENV MKL_THREADING_LAYER SEQUENTIAL


ENTRYPOINT nohup bash -c "avahi-daemon --no-rlimits &"  && \
           bash
